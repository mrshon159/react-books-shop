import React from "react";
import {Menu, Popup, List, Button, Image, Modal, Header, Icon} from 'semantic-ui-react'

const CartComponent = ({ title, id, image, removeFromCart }) => (
    <List selection verticalAlign='middle'>
        <List.Item>
            <List.Content floated='right'>
                <Button color='red' onClick={removeFromCart.bind(this, id)}>Удалить</Button>
            </List.Content>
            <Image avatar src={image} />
            <List.Content>{title}</List.Content>
        </List.Item>
    </List>
)

const MenuComponent = ({ totalPrice, count, items }) => {
    const [open, setOpen] = React.useState(false)
    return (
        <Menu>
            <Menu.Item name="browse">Магазин книг</Menu.Item>
            <Menu.Menu position="right">
                <Menu.Item name="browse">Итого: &nbsp; <b>{totalPrice}</b><i className="ruble sign icon"></i></Menu.Item>
                <Modal
                    open={open}
                    trigger=
                        {
                            <Menu.Item name="browse">Корзина (<b>{count}</b>)</Menu.Item>
                        }
                    onClose={() => setOpen(false)}
                    onOpen={() => setOpen(true)}
                >
                    <Modal.Header>Ваша корзина</Modal.Header>
                    <Modal.Content image>

                        <Modal.Description>
                            {items.length != 0 ?
                                <p>{items.map(book => <CartComponent {...book} />)}</p> :
                                <p>В корзине пока нет товаров</p>}
                        </Modal.Description>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button color='red' onClick={() => setOpen(false)}>
                            <Icon name='remove' /> Закрыть корзину
                        </Button>
                        <Button color='green' onClick={() => setOpen(false)}>
                            <Icon name='checkmark' /> Оплатить заказ
                        </Button>
                    </Modal.Actions>
                </Modal>
            </Menu.Menu>
        </Menu>
    )
}

export default MenuComponent
