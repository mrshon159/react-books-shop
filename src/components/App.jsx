import React from 'react';
import { Container, Card } from 'semantic-ui-react'
import axios from "axios";

import MenuComponent from "../containers/MenuComponent";
import BookCard from "../containers/bookCart";
import Filter from "../containers/Filter";

class App extends React.Component {
    componentDidMount = () => {
        const { setBooks } = this.props
        axios.get('./books.json').then(({data}) => {
           setBooks(data)
        })
    };

    render() {
        const { books, isReady} = this.props
        return (
            <Container>
                <MenuComponent />
                <Filter />
                <Card.Group itemsPerRow={4}>
                    {
                        !isReady ? 'Загрузка...' :
                            books.map( (book, key) => (
                                <BookCard key={book.id} { ...book }/>
                            ))
                    }
                </Card.Group>
            </Container>
        )
    }
}

export default App;
