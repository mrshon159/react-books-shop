import React from "react";
import { button } from 'semantic-ui-react'

const BookCart = (book) => {

    const { title, author, price, image, addToCart, addedCount } = book

    return (
        <div className="ui card">
            <div className="image">
                <img src={image} />
            </div>
            <div className="content">
                <a className="header">{title}</a>
                <div className="meta">
                    <span className="date">{author}</span>
                </div>
            </div>
            <div className="extra content">
                <a>
                    <i className="ruble sign icon"></i>
                    {price}
                </a>
            </div>
            <button className="ui button" onClick={addToCart.bind(this, book)}>
                Добавить в корзину {addedCount > 0 && `(${addedCount})`}
            </button>
        </div>
    )
}

export default BookCart