import React from "react";
import { Menu, Input } from 'semantic-ui-react'
import {setSearchQuery} from "../actions/filter";

const Filter = ({ setFilter, filterBy, searchQuery, setSearchQuery }) => {

    return (
        <Menu secondary>
            <Menu.Item
                active={filterBy === 'all'}
                onClick={setFilter.bind(this, 'all')}
            >Все</Menu.Item>
            <Menu.Item
                active={filterBy === 'price_high'}
                onClick={setFilter.bind(this, 'price_high')}
            >Цена (сначала высокая)</Menu.Item>
            <Menu.Item
                active={filterBy === 'price_low'}
                onClick={setFilter.bind(this, 'price_low')}
            >Цена (сначала низкая)</Menu.Item>
            <Menu.Item
                active={filterBy === 'author'}
                onClick={setFilter.bind(this, 'author')}
            >Автор</Menu.Item>
            <Menu.Item>
                <Input
                    onChange={e => setSearchQuery(e.target.value)}
                    icon="search"
                    value={searchQuery}
                    placeholder='Введите название...'></Input>
            </Menu.Item>
        </Menu>
    )
}

export default Filter
